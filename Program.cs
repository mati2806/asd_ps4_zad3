﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace ZAD3_PS_4
{
    class Program
    {
        public static avlTree tree = new avlTree();
        
        static void Main(string[] args)
        {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            string fileName = "in3.txt";
            string text;
            ConsoleKeyInfo key;

            try { LoadAndExecute(fileName); }
            catch (FileNotFoundException)
            {
                Console.WriteLine("Nie znaleziono pliku o nazwie: " + fileName);
                Console.WriteLine();
                
            }

            watch.Stop();

            Console.WriteLine("Wykonano w {0} ms", watch.ElapsedMilliseconds);

            Console.ReadKey(true);
            while (true)
            {
                Console.Clear();
                Console.WriteLine("Wybierz opcje: ");
                Console.WriteLine("1 Dodaj element");
                Console.WriteLine("2 Usun element");
                Console.WriteLine("3 Wyszukaj slowo");
                Console.WriteLine("4 Wyszukaj prefix");
                Console.WriteLine("5 Wypisz drzewo");

                key = Console.ReadKey(true);


                switch (key.KeyChar.ToString())
                {
                    
                    case "1":
                        Console.Clear();
                        Console.Write("Dodaj:");
                        text = Console.ReadLine();
                        tree.Add(text);
                        break;
                    
                    case "2":
                        Console.Clear();
                        Console.Write("Usun:");
                        text = Console.ReadLine();
                        tree.Delete(text);
                        break;
                    
                    case "3":
                        Console.Clear();
                        Console.Write("Szukaj:");
                        text = Console.ReadLine();
                        Console.Write("Czy slowo {0} jest:", text);
                        if (tree.Search(text)) Console.Write("TAK");
                        else Console.Write("NIE");
                        Console.ReadKey(true);
                        break;
                    
                    case "4":
                        Console.Clear();
                        Console.Write("Szukaj prefixu:");
                        text = Console.ReadLine();
                        Console.WriteLine("Ilosc slow z prefixem {0}:{1} ", text, tree.Prefix(text));
                        Console.ReadKey(true);
                        break;
                    
                    case "5":
                        Console.Clear();
                        tree.DisplayTree();
                        Console.ReadKey(true);
                        break;
                        
                   

                }

                



            }

                      
        }

        static void LoadAndExecute(string fileName)
        {
            int i;  
            string[] words = new string[] { };

            StreamReader reader = new StreamReader(fileName);
            StreamWriter writer = new StreamWriter("out.txt");

            words = reader.ReadToEnd().Split(new[] { '\n', ' '});

     

            Console.WriteLine("Ilosc komend: {0}", words[0]);
            Console.WriteLine();

            for (i = 1; i < words.Length-1; i += 2)
            {
                  
                switch(words[i])
                {
                    case "W":
                        //Console.WriteLine("Dodaje {0}", words[i + 1].Trim());
                        tree.Add(words[i + 1].Trim());
                        break;

                    case "U":
                        //Console.WriteLine("Usuwam {0}", words[i + 1].Trim());
                        tree.Delete( words[i + 1].Trim());
                        break;

                    case "S":
                        //Console.WriteLine("Szukam {0} wynik:{1}", words[i + 1].Trim(), tree.Search(words[i + 1].Trim()));
                        if (tree.Search(words[i + 1].Trim()))
                        {
                            Console.WriteLine("TAK");
                            writer.WriteLine("TAK");
                        }
                        else
                        {
                            Console.WriteLine("NIE");
                            writer.WriteLine("NIE");
                        }
                        break;

                    case "L":
                        //Console.WriteLine("Szukam slow o prefiksie {0} znaleziono: {1}", words[i + 1].Trim(), tree.Prefix(words[i + 1].Trim()));
                        int count = tree.Prefix(words[i + 1].Trim());
                        Console.WriteLine(count);
                        writer.WriteLine(count);
                        break;

                    default:
                        //Console.WriteLine("Komenda nie istnieje!");
                        break;

                }
           
            }
            reader.Close();
            writer.Close();




        }
    }
}
