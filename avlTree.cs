﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZAD3_PS_4
{
    class avlTree
    {
        public class Node
        {
            public string data;
            public int height;
            public Node left;
            public Node right;
            public Node(string data)
            {
                this.data = data;
            }
        }
        Node root;
        int counter = 0;


        public void Add(string data)
        {
            Node newNode = new Node(data);

            if (root == null)
            {
                root = newNode;
            }
            else
            {
                root = Insert(root, newNode);
            }
        }
        private Node Insert(Node tmp, Node newNode)
        {
            if (tmp == null)
            {
                tmp = newNode;
               
            }
            else if (newNode.data.CompareTo(tmp.data) > 0)
            {
                tmp.left = Insert(tmp.left, newNode);
                tmp = BalanceTree(tmp);
            }
            else if (newNode.data.CompareTo(tmp.data) < 0)
            {
                tmp.right = Insert(tmp.right, newNode);
                tmp = BalanceTree(tmp);
            }
            return tmp;
        }
        

        public void Delete(string target)
        {
    
            if(root == null)
            {
                return;
            }
            else
            root = Delete(root, target);
        }

        
        public Node Delete(Node tmp, string word)
        {
        
            if (tmp == null)
            {
                return null;
            } 
            else if (tmp.data.CompareTo(word) < 0)
            {
                tmp.left = Delete(tmp.left, word);
                tmp = BalanceTree(tmp); //balansujemy element
            }
            else if (tmp.data.CompareTo(word) > 0)
            {
                tmp.right = Delete(tmp.right, word);
                tmp = BalanceTree(tmp); //balansujemy element
            }    
            else
            {          
                if ( (tmp.left != null && tmp.right == null) || (tmp.left == null && tmp.right == null) || (tmp.left == null && tmp.right != null) ) //sprawdzenie dla max 1 syna
                {                   
                    if (tmp.right != null) tmp = tmp.right;
                    else tmp = tmp.left;
                }
                else
                {
                    Node current = tmp.right;
                    while (current.left != null)
                    {
                        current = current.left;
                    }
                       

                    Node temp = current;
                    tmp.data = temp.data;              
                    tmp.right = Delete(tmp.right, temp.data);
                }
            }

            if (tmp == null) return null; //jeżeli liść
            tmp = BalanceTree(tmp); //balansujemy element
            return tmp;
        }
 
        private int getHeight(Node tmp) //Obliczanie wysokości danego poddrzewa
        {
            int leftHeight=0, rightHeight=0;
            int height = 0;
            
            if (tmp != null)
            {
                if(tmp.left != null) leftHeight = tmp.left.height;
                if (tmp.right != null) rightHeight = tmp.right.height;
         
                height = Math.Max(leftHeight, rightHeight) + 1; //wysokość podrzewa 

                tmp.height = height; //zapisujemy do węzła
            }
            
            return height;
        }


        private int getBalanceFactor(Node tmp) //Obliczanie wagi węzła
        { 
            int balanceFactor = getHeight(tmp.left) - getHeight(tmp.right);
            return balanceFactor;
        }

        private Node BalanceTree(Node tmp) //Funkcja balansująca drzewo
        {
            int balanceFactor = getBalanceFactor(tmp);
            if (balanceFactor == 2) //jeżeli waga aktualnego węzła = 2
            {
                if (getBalanceFactor(tmp.left) > 0) 
                {
                    tmp = RotationRR(tmp); //RR
                }
                else
                {
                    tmp = RotationLR(tmp);
                }
            }
            else if (balanceFactor == -2)
            {
                if (getBalanceFactor(tmp.right) > 0)
                {
                    tmp = RotationRL(tmp);
                }
                else
                {
                    tmp = RotationLL(tmp); //LL
                }
            }
            return tmp;
        }
       
        private Node RotationLR(Node A)
        {
            //Console.WriteLine("LR");
            Node B = A.left;
            A.left = RotationLL(B);
            return RotationRR(A);
        }
        private Node RotationRL(Node A) //obrót Prawo-Lewo 
        {
            //Console.WriteLine("RL");
            Node B = A.right;
            A.right = RotationRR(B);
            return RotationLL(A);
        }
        private Node RotationLL(Node A) //obrót Lewo
        {
            //Console.WriteLine("LL");
            Node B = A.right;
            A.right = B.left;
            B.left = A;
            return B;
        }
        private Node RotationRR(Node A) //obrót Prawo
        {
            //Console.WriteLine("RR");
            Node B = A.left;
            A.left = B.right;
            B.right = A;
            return B;
        }


        public bool Search(string target) //Complete
        {
            Node tmp = root;

            while (tmp != null)
            {
                if (tmp.data == target) return true;

                if (tmp.data.CompareTo(target) < 0 )
                {
                    //Console.WriteLine("Jestem w {0}, ide do {1}", tmp.data, tmp.left.data);
                    tmp = tmp.left;

                }
                else if (tmp.data.CompareTo(target) > 0)
                {
                    //Console.WriteLine("Jestem w {0}, ide do {1}", tmp.data, tmp.right.data);
                    tmp = tmp.right;

                }
            }
            return false;
        }

        public int Prefix(string target) //Complete
        {
            int wordsFound = 0;
            wordsFound = PrefixRecursive(target, root);

            return wordsFound;
        }

        private int PrefixRecursive(string target, Node tmp)
        {
            
            int wordsFound = 0;

            if (tmp.data.StartsWith(target)) wordsFound++;

            if (tmp.left != null) wordsFound += PrefixRecursive(target, tmp.left);
            if (tmp.right != null) wordsFound += PrefixRecursive(target, tmp.right);

            return wordsFound;


        }

        public void DisplayTree()
        {
            if (root == null)
            {
                Console.WriteLine("Drzewo jest puste!");
                return;
            }
            RecursiveDisplayTree(root);
            Console.WriteLine();
        }

        private void RecursiveDisplayTree(Node current)
        {
            counter++;
            int i;

            if (current != null)
            {
                for (i = 0; i < counter - 1; i++)
                {                   
                        Console.Write('\t');      
                }
                Console.WriteLine(current.data);
                RecursiveDisplayTree(current.right);
                RecursiveDisplayTree(current.left);

            }
            else
            {
                for (i = 0; i < counter - 1; i++)
                {
                    Console.Write('\t');
                }
                Console.WriteLine("(...)");

            }
            counter--;
        }
    }
}

